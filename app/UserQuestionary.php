<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestionary extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'promotionsSuggestions', 'name', 'email', 'phone',
    ];
}
