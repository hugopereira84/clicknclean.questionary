<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreQuestionaryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promotionsSuggestions' => 'required|min:5|max:255',
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            'promotionsSuggestions.required' => 'A title is required',
            'name.required'  => 'A message is required',
            'email.required'  => 'A message is required',
            'phone.required'  => 'A message is required',
        ];
    }
}
