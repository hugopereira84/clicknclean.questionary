<!-- resources/views/tasks.blade.php -->


<!-- https://laravel.com/docs/5.2/quickstart#building-layouts-and-views -->
@extends('layouts.app')

@section('content')
    <style type="text/css">
        .form-group .radio{text-align: center; display:inline-block; margin: 0 15px}

        .form-group .radio label{display:block; padding-left: 0px}
        .form-group .radio input[type=radio]{position: relative; margin: auto}
        .form-group.inlineElements{    margin-bottom: 15px;}

        .form-group .textarea, .form-horizontal h4{ margin: 0 15px}
        .form-horizontal h4, .form-group h4{
            color: #1b809e;
        }
    </style>
    <div class="wrapper">
        <!-- general form elements disabled -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Click and clean</h3>
            </div>


            {!! Form::open(array('url'=>url('questionary'),'method'=>'POST', 'class'=>"form-horizontal")) !!}

                {!! csrf_field() !!}


                        <!-- radio -->
                <div class="box-body">
                    <div class="form-group">
                        <h4>Como conheceu a Click n Clean?</h4>

                        <div class="radio">
                            <label>
                                Facebook
                            </label>
                            <input type="radio" name="conhecer" id="conhecerFacebook" value="Facebook">
                        </div>
                        <div class="radio">
                            <label>
                                WebSite
                            </label>
                            <input type="radio" name="conhecer" id="conhecerWebSite" value="WebSite">
                        </div>
                        <div class="radio">
                            <label>
                                Flyer Publicitário
                            </label>
                            <input type="radio" name="conhecer" id="conhecerFlyerPublicitario" value="FlyerPublicitario">
                        </div>
                        <div class="radio">
                            <label>
                                Viu os cacifos CnC
                            </label>
                            <input type="radio" name="conhecer" id="conhecerViuOsCacifosCnC" value="ViuOsCacifosCnC">
                        </div>
                        <div class="radio">
                            <label>
                                Outro
                            </label>
                            <input type="radio" name="conhecer" id="conhecerOutro" value="Outro">
                        </div>
                    </div>
                </div>


                <div class="box-body">
                    <div class="form-group">
                        <h4>Qual/quais os serviços que usa mais?</h4>

                        <div class="radio">
                            <label>Limpeza a Seco</label>
                            <input type="radio" name="servicos" id="servicosLimpezaASeco" value="LimpezaASeco">
                        </div>
                        <div class="radio">
                            <label>Lavar & Engomar</label>
                            <input type="radio" name="servicos" id="servicosLavarEEngomar" value="LavarEEngomar">
                        </div>
                        <div class="radio">
                            <label>Engomar</label>
                            <input type="radio" name="servicos" id="servicosEngomar" value="Engomar">
                        </div>
                        <div class="radio">
                            <label>Sapateiro</label>
                            <input type="radio" name="servicos" id="servicosSapateiro" value="Sapateiro">
                        </div>
                        <div class="radio">
                            <label>Costura</label>
                            <input type="radio" name="servicos" id="servicosCostura" value="Costura">
                        </div>
                        <div class="radio">
                            <label>Outro</label>
                            <input type="radio" name="servicos" id="servicosOutro" value="Outro">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <h4>Quantas vezes por mês usa os nosso serviços?</h4>

                        <div class="radio">
                            <label>1</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                        </div>
                        <div class="radio">
                            <label>2</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        </div>
                        <div class="radio">
                            <label>3</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                        <div class="radio">
                            <label>4</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                        <div class="radio">
                            <label>5</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <h4>Porquê?</h4>

                        <div class="radio">
                            <label>Não necessita</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                        </div>
                        <div class="radio">
                            <label>Preço</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        </div>
                        <div class="radio">
                            <label>Distância do cacifo</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                        <div class="radio">
                            <label>Outro</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <h4>Sente-se satisfeito com o serviço?</h4>

                        <div class="radio">
                            <label>Muito<br> Mau</label>
                        </div>
                        <div class="radio">
                            <label>1</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                        </div>
                        <div class="radio">
                            <label>2</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        </div>
                        <div class="radio">
                            <label>3</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                        <div class="radio">
                            <label>4</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                        <div class="radio">
                            <label>5</label>
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                        </div>
                        <div class="radio">
                            <label>Muito<br> Bom</label>
                        </div>
                    </div>
                </div>


                <div class="box-body">
                    <div class="form-group">
                        <h4>Nível de cortesia dos colaboradores?</h4>
                        <div class="radio">
                            <label class="text-left">Muito<br> Mau</label>
                        </div>
                        <div class="radio">
                            {{ Form::label('levelCourtesy', '1' )}}
                            {{ Form::radio('levelCourtesy', '1')}}
                        </div>
                        <div class="radio">
                            {{ Form::label('levelCourtesy', '2' )}}
                            {{ Form::radio('levelCourtesy', '2')}}
                        </div>
                        <div class="radio">
                            {{ Form::label('levelCourtesy', '3' )}}
                            {{ Form::radio('levelCourtesy', '3')}}
                        </div>
                        <div class="radio">
                            {{ Form::label('levelCourtesy', '4' )}}
                            {{ Form::radio('levelCourtesy', '4')}}
                        </div>
                        <div class="radio">
                            {{ Form::label('levelCourtesy', '5' )}}
                            {{ Form::radio('levelCourtesy', '5')}}
                        </div>
                        <div class="radio">
                            <label class="text-right">Muito<br> Bom</label>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    {{ Form::label('promotionsSuggestions', 'Sugestões e promoções mais desejadas?', ['class' => 'control-label'] )}}
                    {{ Form::textarea('promotionsSuggestions', null , ['class'=>'form-control',
                                                                        'id'=>'name', 'rows'=>'5',
                                                                        'placeholder'=>'Introduza as suas sugestões']) }}

                    @if ( !empty($errors) && $errors->has('promotionsSuggestions') )
                        <p style="color:red;">{!!$errors->first('promotionsSuggestions')!!}</p>
                    @endif
                </div>

                <div class="box-body">
                    <div class="form-group inlineElements">
                        {{Form::label('name', 'Nome', ['class' => 'col-sm-1 control-label'] )}}

                        <div class="col-sm-11">
                            {{ Form::text('name', null , ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Introduza o seu nome']) }}
                        </div>

                        @if ( isset($errors) && !empty($errors) && $errors->has('name'))
                            <p style="color:red;">{!!$errors->first('name')!!}</p>
                        @endif
                    </div>
                    <div class="form-group inlineElements">
                        {{Form::label('email', 'Email', ['class' => 'col-sm-1 control-label'] )}}

                        <div class="col-sm-11">
                            {{ Form::text('email', null , ['class'=>'form-control', 'id'=>'email', 'placeholder'=>'Introduza o seu email']) }}
                        </div>

                        @if ( isset($errors) && !empty($errors) && $errors->has('email'))
                            <p style="color:red;">{!!$errors->first('email')!!}</p>
                        @endif
                    </div>
                    <div class="form-group inlineElements">
                        {{Form::label('phone', 'Telefone', ['class' => 'col-sm-1 control-label'] )}}

                        <div class="col-sm-11">
                            {{ Form::text('phone', null , ['class'=>'form-control', 'id'=>'phone', 'placeholder'=>'Introduza o seu telefone']) }}
                        </div>

                        @if ( isset($errors) && !empty($errors) && $errors->has('phone'))
                            <p style="color:red;">{!!$errors->first('phone')!!}</p>
                        @endif
                    </div>
                </div>


                <h4 class="info">A sua satisfação é o mais importante para nós. Obrigado pela sua opinião.</h4>

                <div class="box-footer">
                    {{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
                </div>
            {!! Form::close() !!}



        </div>
        <!-- /.box -->

    </div>
@endsection
